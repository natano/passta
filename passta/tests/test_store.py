import os
import unittest

import passta
from passta.store import Store


class TestStore(unittest.TestCase):
    def test_load_new(self):
        store = Store(os.devnull)
        self.assertEqual(store['passwords'], {})
        self.assertEqual(store['__passta']['version'], passta.__version__)
        self.assertEqual(store['__passta']['store_version'], store.VERSION)

    @unittest.skip
    def test_load_existing(self):
        raise NotImplementedError

    @unittest.skip
    def test_load_upgrade_from_version_0(self):
        raise NotImplementedError
