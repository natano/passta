import os

from passta.testcases import PopenTestcase
from passta.clipboard import which, copy, ClipboardError


class TestWhich(PopenTestcase):
    def test_returns_empty_list_when_file_is_not_found(self):
        self.assertEqual(list(which('non-existent')), [])

    def test_does_not_pick_up_files_that_are_not_executable(self):
        ls = self.create_sh('ls', mode=0o600)
        self.assertEqual(list(which('ls')), [])
        os.chmod(ls, 0o755)
        self.assertEqual(list(which('ls')), [ls])

    def test_returns_more_than_one_match_when_available(self):
        self.path_append('bin')
        ls = self.create_sh('ls')
        self.assertEqual(list(which('ls')), [ls])
        bin_ls = self.create_sh('bin/ls')
        self.assertEqual(list(which('ls')), [ls, bin_ls])


class TestClipboard(PopenTestcase):
    def test_raises_ClipboardError_when_no_clipboard_manager_is_found(self):
        with self.assertRaises(ClipboardError):
            copy('something')

    def test_copy(self):
        for name in ('xclip', 'pbcopy'):
            self.create_sh_and_check_output(name)

    def create_sh_and_check_output(self, name):
        self.create_sh(name, '/bin/cat >{}/out.txt'.format(self.tmp_dir))
        copy('something')
        with open(os.path.join(self.tmp_dir, 'out.txt'), 'r') as f:
            self.assertEqual(f.read(), 'something')

    def test_copy_nonzero(self):
        for name in ('xclip', 'pbcopy'):
            self.raises_error_on_nonzero(name)

    def raises_error_on_nonzero(self, name):
        self.create_sh(name, 'exit 1')

        with self.assertRaises(ClipboardError):
            copy('something')
